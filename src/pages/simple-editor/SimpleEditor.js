import React, {Component} from 'react';
import './SimpleEditor.scss';
import logo from '../../assets/img/logo.svg';
import BackgroundSelector from "../../modules/simple-editor/components/BackgroundSelector/BackgroundSelector";
import {withEmit} from "react-emit";
import {backInHistory, load, save} from "../../modules/simple-editor/store/actions";
import connect from "react-redux/es/connect/connect";
import LogoSelector from "../../modules/simple-editor/components/LogoSelector/LogoSelector";
import TextSelector from "../../modules/simple-editor/components/TextSelector/TextSelector";
import DrawableArea from "../../modules/simple-editor/components/DrawableArea/DrawableArea";
import Button from "../../components/Button/Button";
import {EVENT_DOWNLOAD_CANVAS_IMAGE} from "../../commons/event-emiter-actions";
import Icon from "../../components/Icon/Icon";

type Props = {
    emit: any,
    historyLength: number,
    backInHistory: any,
    save: any,
    load: any
};

class SimpleEditor extends Component<Props> {
    downloadImage = () => {
        this.props.emit(EVENT_DOWNLOAD_CANVAS_IMAGE);
    };

    render() {
        return (
            <div id={'simple-editor-page'}>
                <header>
                    <img src={logo} alt="logo"/>
                    <div className={'action-buttons'}>
                        {this.props.historyLength > 1 &&
                        <Button onClick={this.props.backInHistory}>
                            <Icon icon={'undo'} />
                        </Button>
                        }

                        <Button text={'Save'} onClick={this.props.save}/>

                        {this.props.hasSavedState &&
                        <Button text={'Load'} onClick={this.props.load}/>
                        }
                    </div>
                </header>
                <div className={'layout-holder'}>
                    <div className={'left-menu'}>
                        <BackgroundSelector/>
                    </div>
                    <div className={'center-area'}>
                        <DrawableArea ref={'canvas'} className={'canvas-area'}/>

                        <div className={'center-align'}>
                            <Button onClick={this.downloadImage} text={'Download'}/>
                        </div>
                    </div>
                    <div className={'right-menu'}>
                        <LogoSelector className={'logo-selector'}/>
                        <TextSelector/>
                    </div>
                </div>
            </div>
        );
    }
}

SimpleEditor = withEmit(SimpleEditor);

const mapStateToProps = (state) => {
    return {
        historyLength: state.simpleEditor.drawableHistory.length,
        hasSavedState: state.simpleEditor.hasSavedState
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        backInHistory: () => dispatch(backInHistory()),
        save: () => dispatch(save()),
        load: () => dispatch(load())
    };
};

SimpleEditor = connect(mapStateToProps, mapDispatchToProps)(SimpleEditor);

export default SimpleEditor;
