import React, {Component} from 'react';
import SimpleEditor from './pages/simple-editor/SimpleEditor';
import ContextMenu from './modules/context-menu/components/ContextMenu';

class App extends Component {
    render() {
        return (
            <div>
                <ContextMenu/>
                <SimpleEditor/>
            </div>
        );
    }
}

export default App;
