import React, {Component} from 'react';

type Props = {
	icon: string
};

class Icon extends Component<Props> {
	render() {
		return <i className={['fas', 'fa-' + this.props.icon].join(' ')}></i>
	}
}

export default Icon;
