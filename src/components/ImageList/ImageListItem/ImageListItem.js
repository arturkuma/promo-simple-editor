import React, {Component} from 'react';
import './ImageListItem.scss';
import {isFunction} from 'lodash';

type Props = {
	onClick?: any,
     src: string,
     width?: string,
     height?: string,
};

class ImageListItem extends Component<Props> {
    onClick = () => {
        if (isFunction(this.props.onClick)) {
            this.props.onClick(this.props.src)
        }
    };

    render() {
        return (
            <div
                onClick={this.onClick}
                className={'image-list-item'}
                style={{
                    width: this.props.width || '100%',
                    height: this.props.height || '100%',
                    'backgroundImage': 'url("' + this.props.src + '")'
                }}/>
        );
    }
}

export default ImageListItem;
