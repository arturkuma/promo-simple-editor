import React, {Component} from 'react';
import './ImageList.scss';
import {fill} from 'lodash';

type Props = {
	columns: number,
	gridGap?: string
};

class ImageList extends Component<Props> {
	render() {
		return (
			<div
				className={'image-list'}
				style={{
					gridTemplateColumns: fill(Array(this.props.columns), 'auto').join(' '),
					gridGap: this.props.gridGap || '10px'
				}}>
				{this.props.children}
			</div>
		);
	}
}

export default ImageList;
