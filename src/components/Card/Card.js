import React, {Component} from 'react';
import './Card.scss';

type Props = {
	title: any,
	className?: string
};

class Card extends Component<Props> {
	render() {
		return (
			<div id={'card'} className={this.props.className}>
				<h2 className={'card_header'}>{this.props.title}</h2>
				{this.props.children}
			</div>
		);
	}
}

export default Card;
