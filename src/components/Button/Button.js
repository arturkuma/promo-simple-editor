import React, {Component} from 'react';
import './Button.scss';
import {isFunction} from 'lodash';

type Props = {
	onClick?: void,
	text?: any
};

class Button extends Component<Props> {
	onClick = () => {
		if (isFunction(this.props.onClick)) {
			this.props.onClick();
		}
	};

	render() {
		return (
			<button onClick={this.onClick} className={'button-primary'}>{this.props.text || this.props.children}</button>
		);
	}
}

export default Button;
