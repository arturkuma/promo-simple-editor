import React, {Component} from 'react';
import './Radio.scss';

type Props = {
	checked?: boolean,
	name: string,
	value?: any,
	onChange?: any
};

class Radio extends Component<Props> {
	render() {
		return (
			<div className={'radio-button'}>
				<label>
					<input
						type='radio'
						checked={this.props.checked}
						name={this.props.name}
						value={this.props.value}
						onChange={this.props.onChange}
					/>

					{this.props.children}
				</label>
			</div>
		);
	}
}

export default Radio;
