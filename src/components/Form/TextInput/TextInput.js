import React, {Component} from 'react';
import './TextInput.scss';
import {classNameObjectParse} from '../../../commons/functions/classNameObjectParse';
import {isNil} from 'lodash';

type Props = {
	value?: any,
	title: any
};

class TextInput extends Component<Props> {
	constructor(props) {
		super(props);

		this.state = {
			value: ''
		};
	}

	handleChange = (e) => {
		this.props.onChange(e.target.value);

		this.setState({
			value: e.target.value
		});
	};

	render() {
		const value = (!isNil(this.props.value)) ? this.props.value : this.state.value;

		return (
			<div className={'form-control'}>
				<input
					onChange={this.handleChange}
					value={value}
					className={classNameObjectParse({
						'text-input': true,
						'active': value !== ''
					})}
					type='text'
				/>

				<label>{this.props.title}</label>
			</div>
		);
	}
}

export default TextInput;
