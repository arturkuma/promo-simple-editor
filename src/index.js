import React from 'react';
import ReactDOM from 'react-dom';
import './assets/styles/index.scss';
import store from './store/store';
import Provider from 'react-redux/es/components/Provider';
import { EmitProvider } from 'react-emit';
import App from './App';

ReactDOM.render(
    <EmitProvider>
        <Provider store={store}>
            <App/>
        </Provider>
    </EmitProvider>
    , document.getElementById('root')
);
