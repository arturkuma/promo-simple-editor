import { combineReducers } from 'redux'

import {reducer as simpleEditor} from '../modules/simple-editor/store/reducer';
import {reducer as contextMenu} from '../modules/context-menu/store/reducer';

export default combineReducers({
    simpleEditor,
    contextMenu
});
