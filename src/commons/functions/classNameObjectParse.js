import {forIn} from "lodash";

export function classNameObjectParse(object) {
    const classess = [];

    forIn(object, (value, key) => {
        if (value) {
            classess.push(key);
        }
    });

    return classess.join(' ');
}