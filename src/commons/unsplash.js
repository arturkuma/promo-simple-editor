import Unsplash, {toJson} from 'unsplash-js';
import {UNSPLASH_CREDENTIALS} from '../config';

const {applicationId, secret} = UNSPLASH_CREDENTIALS;

const unsplash = new Unsplash({
	applicationId,
	secret
});

export async function getRandomPhotoUrl(query, {width, height}) {
	const {urls: {custom}} = await toJson(await unsplash.photos.getRandomPhoto({
		width,
		height,
		query
	}));

	return custom;
}
