let typingTimer;

export function debounce(func, timeout) {
    clearTimeout(typingTimer);

    typingTimer = setTimeout(() => {
        func();
    }, timeout)
}