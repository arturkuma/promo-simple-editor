import React, {Component} from 'react';
import './ContextMenu.scss';
import {map, uniqueId} from 'lodash';
import connect from 'react-redux/es/connect/connect';
import {hideContextMenu} from '../store/actions';

type Props = {
	hideContextMenu: any,
	active: boolean,
	position: {
		x: number,
		y: number
	},
	options: [{
		label: any,
		onClick: any
	}]
};

class ContextMenu extends Component<Props> {
	overlayClick = () => {
		this.props.hideContextMenu();
	};

	elementClick = (e, onClick) => {
		this.props.hideContextMenu();
		onClick(e);
	};

	render() {
		if (this.props.active) {
			return (
				<div id={'context-menu-overlay'} onClick={this.overlayClick}>
					<div id={'context-menu'} style={{
						top: this.props.position.y,
						left: this.props.position.x
					}}>
						{map(this.props.options, ({label, onClick}) => {
							return <div key={uniqueId()} onClick={e => this.elementClick(e, onClick)}>{label}</div>;
						})}
					</div>
				</div>
			);
		} else {
			return null;
		}
	}
}

const mapStateToProps = (state) => {
	return {
		active: state.contextMenu.options.length > 0,
		options: state.contextMenu.options,
		position: state.contextMenu.position
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		hideContextMenu: () => dispatch(hideContextMenu())
	};
};

ContextMenu = connect(mapStateToProps, mapDispatchToProps)(ContextMenu);

export default ContextMenu;
