import {
    ACTION_HIDE_CONTEXT_MENU,
    ACTION_SHOW_CONTEXT_MENU
} from "../../../store/actions";

export const reducer = (state = {
    position: {
        x: 0,
        y: 0
    },
    options: []
}, action) => {
    return ({
        [ACTION_SHOW_CONTEXT_MENU]: () => {
            return action.state;
        },
        [ACTION_HIDE_CONTEXT_MENU]: () => {
            return {
                options: []
            };
        }
    }[action.type] || (() => {
        return state
    }))();
};
