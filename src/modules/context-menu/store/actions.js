import {ACTION_HIDE_CONTEXT_MENU, ACTION_SHOW_CONTEXT_MENU} from "../../../store/actions";

export function showContextMenu(position, options) {
    return {
        type: ACTION_SHOW_CONTEXT_MENU,
        state: {
            position,
            options
        }
    }
}

export function hideContextMenu() {
    return {
        type: ACTION_HIDE_CONTEXT_MENU
    }
}
