export function getLatestCanvasHistory(state) {
    return state.simpleEditor.drawableHistory[state.simpleEditor.drawableHistory.length - 1];
}
