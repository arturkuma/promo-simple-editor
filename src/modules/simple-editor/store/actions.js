import {
    ACTION_ADD_OBJECT,
    ACTION_BACK_IN_HISTORY, ACTION_CLONE_LATEST_HISTORY_STATE,
    ACTION_DELETE_BACKGROUND, ACTION_DELETE_OBJECT, ACTION_LOAD, ACTION_SAVE,
    ACTION_SET_BACKGROUND, ACTION_UPDATE_OBJECT
} from '../../../store/actions';
import {OBJECT_TYPE_IMAGE, OBJECT_TYPE_TEXT} from './reducer';
import {isNil} from 'lodash';

export function setBackground(url) {
    return {
        type: ACTION_SET_BACKGROUND,
        url
    }
}

export function addLogo(src) {
    return {
        type: ACTION_ADD_OBJECT,
        object: {
            type: OBJECT_TYPE_IMAGE,
            image: {
                src
            },
            x: 0,
            y: 0,
            width: 100,
            height: 100
        }
    }
}

export function deleteBackground() {
    return {
        type: ACTION_DELETE_BACKGROUND
    }
}

export function addText(value, fontFamily, color) {
    return {
        type: ACTION_ADD_OBJECT,
        object: {
            type: OBJECT_TYPE_TEXT,
            text: {
                value,
                fontFamily,
                fontSize: 20,
                color: color
            },
            x: 0,
            y: 0
        }
    }
}

export function backInHistory() {
    return {
        type: ACTION_BACK_IN_HISTORY
    }
}

export function updateLogo(key, position, dimensions, saveInHistory) {
    const object = {
        type: ACTION_UPDATE_OBJECT,
        saveInHistory: true
    };

    object.object = {
        key
    };

    if(!isNil(position)) {
        const {x, y} = position;

        object.object.x = x;
        object.object.y = y;
    }

    if(!isNil(dimensions)) {
        const {width, height} = dimensions;

        object.object.width = width;
        object.object.height = height;
    }

    if(!isNil(saveInHistory)) {
        object.saveInHistory = saveInHistory;
    }

    return object;
}

export function cloneLatestHistoryState() {
    return {
        type: ACTION_CLONE_LATEST_HISTORY_STATE
    }
}

export function deleteObject(key) {
    return {
        type: ACTION_DELETE_OBJECT,
        key
    }
}

export function save() {
    return {
        type: ACTION_SAVE
    }
}

export function load() {
    return {
        type: ACTION_LOAD
    }
}
