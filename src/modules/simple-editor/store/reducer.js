import {
	ACTION_ADD_OBJECT,
	ACTION_BACK_IN_HISTORY, ACTION_CLONE_LATEST_HISTORY_STATE,
	ACTION_DELETE_BACKGROUND, ACTION_DELETE_OBJECT, ACTION_LOAD, ACTION_SAVE,
	ACTION_SET_BACKGROUND, ACTION_UPDATE_OBJECT
} from '../../../store/actions';
import {findIndex} from 'lodash';
import {CANVAS_SIZE, DEFAULT_BACKGROUND_URL} from '../config';

export const OBJECT_TYPE_IMAGE = 'OBJECT_TYPE_IMAGE';
export const OBJECT_TYPE_TEXT = 'OBJECT_TYPE_TEXT';

const defaultState = {
	hasSavedState: localStorage.hasOwnProperty('simpleEditorState'),
	drawableHistory: [
		[
			{
				key: 'background',
				type: OBJECT_TYPE_IMAGE,
				image: {
					src: DEFAULT_BACKGROUND_URL
				},
				x: 0,
				y: 0,
				width: CANVAS_SIZE.width,
				height: CANVAS_SIZE.height
			}
		]
	]
};

export const reducer = (state = defaultState, action) => {

	const latestState = state.drawableHistory[state.drawableHistory.length - 1];

	const pushToHistoryState = (object) => {
		return {
			...state,
			drawableHistory: [
				...state.drawableHistory,
				object
			]
		};
	};

	return ({
		[ACTION_SET_BACKGROUND]: () => {
			const latestStateCopy = [...latestState];

			const backgroundImageObject = {...latestStateCopy[0]};

			backgroundImageObject.image = {
				src: action.url
			};

			latestStateCopy[0] = backgroundImageObject;

			return pushToHistoryState([
				...latestStateCopy
			]);
		},
		[ACTION_ADD_OBJECT]: () => {
			return pushToHistoryState([
				...latestState,
				{
					key: +new Date() + '',
					...action.object
				}
			]);
		},
		[ACTION_DELETE_BACKGROUND]: () => {
			const latestStateCopy = [...latestState];

			const backgroundImageObject = {...latestStateCopy[0]};

			backgroundImageObject.image = {
				src: DEFAULT_BACKGROUND_URL
			};

			latestStateCopy[0] = backgroundImageObject;

			return pushToHistoryState([
				...latestStateCopy
			]);
		},
		[ACTION_BACK_IN_HISTORY]: () => {
			return {
				...state,
				drawableHistory: [
					...state.drawableHistory.slice(0, state.drawableHistory.length - 1)
				]
			};
		},
		[ACTION_UPDATE_OBJECT]: () => {
			const elementIndex = findIndex(latestState, {key: action.object.key});
			let element = {...latestState[elementIndex]};

			const updatedState = [...latestState];

			updatedState[elementIndex] = {
				...element,
				...action.object
			};

			if (action.saveInHistory === true) {
				return pushToHistoryState(updatedState);
			} else {
				const currentHistory = [...state.drawableHistory];
				currentHistory[currentHistory.length - 1] = updatedState;

				return {
					...state,
					drawableHistory: currentHistory
				};
			}
		},
		[ACTION_CLONE_LATEST_HISTORY_STATE]: () => {
			return pushToHistoryState([...latestState])
		},
		[ACTION_DELETE_OBJECT]: () => {
			const elementIndex = findIndex(latestState, {key: action.key});

			const last = [...latestState];

			last.splice(elementIndex, 1);

			return pushToHistoryState([
				...last
			])
		},
		[ACTION_SAVE]: () => {
			localStorage.setItem('simpleEditorState', JSON.stringify(state.drawableHistory));
			return {
				...state,
				hasSavedState: true
			};
		},
		[ACTION_LOAD]: () => {
			if (state.hasSavedState) {
				return {
					...state,
					drawableHistory: JSON.parse(localStorage.getItem('simpleEditorState'))
				};
			}

			return state;
		}
	}[action.type] || (() => {
		return state
	}))();
};
