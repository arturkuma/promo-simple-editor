import React, {Component} from 'react';
import './BackgroundSelector.scss';
import Card from '../../../../components/Card/Card';
import Button from '../../../../components/Button/Button';
import TextInput from '../../../../components/Form/TextInput/TextInput';
import ImageList from '../../../../components/ImageList/ImageList';
import ImageListItem from '../../../../components/ImageList/ImageListItem/ImageListItem';
import {map, fill, forEach, uniqueId} from 'lodash';
import {getLatestCanvasHistory} from '../../commons';
import {deleteBackground, setBackground} from '../../store/actions';
import connect from 'react-redux/es/connect/connect';
import {debounce} from '../../../../commons/debounce';
import {CANVAS_SIZE, DEFAULT_BACKGROUND_URL} from '../../config';
import {getRandomPhotoUrl} from "../../../../commons/unsplash";

type
Props = {
    style? : any,
    setBackgroundImage: any,
    latestCanvas: any,
    deleteBackground: any
};

class BackgroundSelector extends Component<Props> {
    static imagesNumber = 4;

    constructor(props) {
        super(props);

        this.state = {
            images: fill(Array(BackgroundSelector.imagesNumber), DEFAULT_BACKGROUND_URL)
        };
    }

    componentDidMount() {
        this.loadImages();
    }

    onSearchTermChange = (searchTerm) => {
        debounce(() => {
            this.loadImages(searchTerm);
        }, 250);
    };

    loadImages = (searchTerm = '') => {
        forEach(Array(BackgroundSelector.imagesNumber), (_, key) => {
            const {width, height} = CANVAS_SIZE;

		   getRandomPhotoUrl(searchTerm, {width, height}).then(url => {
			   const stateImages = this.state.images;
			   stateImages[key] = url;

			   this.setState({
				   images: stateImages
			   });
             });
        });
    };

    render() {
        return (
            <Card title={'Select Background'} style={{...this.props.style}} className={'background-selector-card'}>
                <div className={'search-area'}>
                    <TextInput onChange={this.onSearchTermChange} title={'Search'}/>
                </div>

                <div className={'image-list-area'}>
                    <ImageList columns={2}>
                        {map(this.state.images, src => {
                            return <ImageListItem
                                key={uniqueId()}
                                onClick={this.props.setBackgroundImage}
                                height={90}
                                src={src}
                            />
                        })}
                    </ImageList>
                </div>

                {this.props.latestCanvas[0].image.src !== DEFAULT_BACKGROUND_URL &&
                <div className={'center-align'}>
                    <Button onClick={this.props.deleteBackground} text={'Delete'}/>
                </div>
                }
            </Card>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        latestCanvas: getLatestCanvasHistory(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setBackgroundImage: (url) => dispatch(setBackground(url)),
        deleteBackground: () => dispatch(deleteBackground())
    };
};

BackgroundSelector = connect(mapStateToProps, mapDispatchToProps)(BackgroundSelector);

export default BackgroundSelector;
