import React, {Component} from 'react';
import './TextSelector.scss';
import Card from '../../../../components/Card/Card';
import TextInput from '../../../../components/Form/TextInput/TextInput';
import Button from '../../../../components/Button/Button';
import FontSelector from './FontSelector/FontSelector';
import {ChromePicker} from 'react-color';
import connect from 'react-redux/es/connect/connect';
import {addText} from '../../store/actions';
import {keys} from 'lodash';

type
Props = {
    addText: any,
    style: any
};

class TextSelector extends Component<Props> {
    static initialState = {
        text: '',
        fontFamily: keys(FontSelector.fontToDisplay)[0],
        color: '#000000'
    };

    constructor(props) {
        super(props);

        this.state = TextSelector.initialState
    }

    onAdd = () => {
        this.props.addText(this.state.text, this.state.fontFamily, this.state.color);

        this.setState(TextSelector.initialState);
    };

    onTextChange = (text) => {
        this.setState({
            text
        });
    };

    onFontSelectionChange = (option) => {
        this.setState({
            fontFamily: option
        });
    };

    onColorChange = ({hex}) => {
        this.setState({
            color: hex
        });
    };

    render() {
        return (
            <Card title={'Add Text'} style={{...this.props.style}} className={'text-selector-card'}>
                <div className={'add-text-area'}>
                    <TextInput value={this.state.text} onChange={this.onTextChange} title={'Type somthing'}/>
                </div>

                <div className={'select-font-area'}>
                    <FontSelector selectedOption={this.state.fontFamily}
                                  onSelectionChange={this.onFontSelectionChange}/>
                </div>

                <ChromePicker color={this.state.color} onChange={this.onColorChange} className={'color-picker'}/>

                {this.state.text !== '' &&
                <div className={'center-align'}>
                    <Button onClick={this.onAdd} text={'Add'}/>
                </div>
                }
            </Card>
        );
    }
}

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {
        addText: (text, fontFamily, color) => dispatch(addText(text, fontFamily, color))
    };
};

TextSelector = connect(mapStateToProps, mapDispatchToProps)(TextSelector);

export default TextSelector;
