import React, {Component} from 'react';
import './FontSelector.scss';
import {map, uniqueId} from 'lodash';
import Radio from '../../../../../components/Form/Radio/Radio';

type Props = {
	selectedOption?: any,
	onSelectionChange?: any
};

class FontSelector extends Component<Props> {
	static fontToDisplay = {
		'Arial': 'Arial',
		'Times New Roman': 'Times New Roman',
		'Open Sans': "'Open Sans', sans-serif",
	};

	render() {
		return (
			<div className={'font-selection-card'}>
				{map(FontSelector.fontToDisplay, (font, fontName) => {
					return (
						<div key={uniqueId()}>
							<Radio
								checked={this.props.selectedOption === fontName}
								value={fontName}
								name={'font-selection'}
								onChange={e => this.props.onSelectionChange(e.target.value)}>

								<span style={{fontFamily: font}}>{fontName}</span>
							</Radio>
						</div>
					);
				})}
			</div>
		);
	}
}

export default FontSelector;
