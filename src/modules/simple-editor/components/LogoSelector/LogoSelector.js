import React, {Component} from 'react';
import './LogoSelector.scss';
import Card from '../../../../components/Card/Card';
import ImageListItem from '../../../../components/ImageList/ImageListItem/ImageListItem';
import ImageList from '../../../../components/ImageList/ImageList';
import {map, uniqueId} from 'lodash';
import connect from 'react-redux/es/connect/connect';
import {addLogo} from '../../store/actions';
import {AVAILABLE_LOGOS} from '../../config';

type Props = {
	style?: any,
     className?: any,
     addLogo?: any
};

class LogoSelector extends Component<Props> {
    render() {
        return (
            <Card title={'Add Logo'} style={{...this.props.style}} className={this.props.className}>
                <ImageList columns={3} gridGap={'5px'}>
                    {map(AVAILABLE_LOGOS, logo => {
                        return <ImageListItem key={uniqueId()} onClick={this.props.addLogo} height={'50px'} src={logo}/>
                    })}
                </ImageList>
            </Card>
        );
    }
}

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {
        addLogo: (url) => dispatch(addLogo(url))
    };
};

LogoSelector = connect(mapStateToProps, mapDispatchToProps)(LogoSelector);


export default LogoSelector;
