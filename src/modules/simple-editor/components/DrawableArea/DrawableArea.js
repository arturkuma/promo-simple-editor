import React, {Component} from 'react';
import './DrawableArea.scss';
import {Stage, Layer, Text, Circle, Group} from 'react-konva';
import CanvasImage from "./Image/Image";
import {map} from "lodash";
import {connect} from 'react-redux';
import {getLatestCanvasHistory} from "../../commons";
import {withEmit} from "react-emit";
import {EVENT_DOWNLOAD_CANVAS_IMAGE} from "../../../../commons/event-emiter-actions";
import {cloneLatestHistoryState, deleteObject, updateLogo} from "../../store/actions";
import {OBJECT_TYPE_IMAGE, OBJECT_TYPE_TEXT} from "../../store/reducer";
import {showContextMenu} from "../../../context-menu/store/actions";
import {CANVAS_SIZE} from "../../config";

type
Props = {
    on: any,
    updateImage: any,
    showContextMenu: any,
    deleteObject: any,
    className: any,
    latestCanvas: any,
    cloneLatestHistoryState: any
};

class DrawableArea extends Component<Props> {
    static draggableRadius = 5;

    constructor(props) {
        super(props);

        this.state = {
            showDraggables: false
        };
    }

    componentDidMount() {
        this.props.on(EVENT_DOWNLOAD_CANVAS_IMAGE, () => {
            this.hideDraggable();

            const downloadURI = (uri, name) => {
                const link = document.createElement("a");
                link.download = name;
                link.href = uri;
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            };

            setTimeout(() => {
                const dataUrl = this.refs.canvas.toDataURL({pixelRatio: 3});

                if (dataUrl === '') {
                    alert(`Sorry, I had problem during creating image. Probably it's because of missing CORS headers.
							PS. This alert will be much nicer :)`)
                } else {
                    downloadURI(dataUrl, 'project.png');
                }
            }, 500);
        });
    }

    showDraggable = () => {
        if (this.state.showDraggables === false) {
            this.setState({
                showDraggables: true
            });
        }
    };

    hideDraggable = () => {
        this.setState({
            showDraggables: false
        });
    };

    handleDragBoundFunc = ({x, y}, {width, height}) => {
        if (x < 0) {
            x = 0;
        }

        if (y < 0) {
            y = 0;
        }

        if (x + width > CANVAS_SIZE.width) {
            x = CANVAS_SIZE.width - width;
        }


        if (y + height > CANVAS_SIZE.height) {
            y = CANVAS_SIZE.height - height;
        }

        return {x, y}
    };

    handleResizeFunc = ({x, y}, key, {x: parentX, y: parentY}) => {
        const minSize = 30;
        const maxSize = 150;

        let width = x + (DrawableArea.draggableRadius * 2) - parentX;
        let height = width;

        if (width < minSize) {
            width = minSize;
            height = width;
        }

        if (width > maxSize) {
            width = maxSize;
            height = width;
        }

        this.updateElement(key + '', null, {width, height}, false);

        return {
            x: parentX + width - (DrawableArea.draggableRadius * 2),
            y: parentY + height - (DrawableArea.draggableRadius * 2)
        }
    };

    handleDragEnd = ({target, target: {attrs: {name}}}) => {
        this.updateElement(name, {x: target.x(), y: target.y()});
    };

    updateElement = (key, position, dimensions, saveInHistory) => {
        this.props.updateImage(key, position, dimensions, saveInHistory);
    };

    handleContextMenu = (e, key) => {
        e.evt.preventDefault();

        const {clientX: x, clientY: y} = e.evt;

        this.props.showContextMenu({x, y}, [
            {
                label: 'Delete', onClick: () => {
                    this.props.deleteObject(key + '');
                }
            }
        ]);
    };

    render() {
        return (
            <Stage
                width={CANVAS_SIZE.width}
                height={CANVAS_SIZE.height}
                ref={'canvas'}
                onMouseEnter={this.showDraggable}
                onMouseLeave={this.hideDraggable}
            >
                <Layer>
                    {map(this.props.latestCanvas, ({key, type, x, y, width, height, image, text}) => {
                        return {
                            [OBJECT_TYPE_IMAGE]: () => {
                                const {src} = image;

                                return (
                                    <Group
                                        key={key}
                                        name={key + ''}
                                        x={x}
                                        y={y}
                                        onDragEnd={this.handleDragEnd}
                                        dragBoundFunc={(pos) => this.handleDragBoundFunc(pos, {
                                            width,
                                            height
                                        })}
                                        draggable
                                    >
                                        <CanvasImage
                                            crop
                                            image={src}
                                            width={width}
                                            height={height}
                                            onContextMenu={(e) => {
                                                if (key !== 'background') {
                                                    this.handleContextMenu(e, key);
                                                }
                                            }}
                                        />

                                        {key !== 'background' && this.state.showDraggables &&
                                        <Circle
                                            x={width - (DrawableArea.draggableRadius * 2)}
                                            y={height - (DrawableArea.draggableRadius * 2)}
                                            radius={DrawableArea.draggableRadius}
                                            fill={'#F8F8F8'}
                                            stroke={'black'}
                                            onMouseOver={() => {
                                                this.refs.canvas.content.style.cursor = 'nwse-resize';
                                                this.showDraggable();
                                            }}
                                            onMouseOut={() => {
                                                this.refs.canvas.content.style.cursor = 'default';
                                            }}
                                            dragBoundFunc={(pos) => this.handleResizeFunc(pos, key, {
                                                x,
                                                y
                                            })}
                                            onDragStart={this.props.cloneLatestHistoryState}
                                            draggable
                                        />
                                        }
                                    </Group>
                                );
                            },
                            [OBJECT_TYPE_TEXT]: () => {
                                const {fontFamily, fontSize, value, color} = text;

                                return <Text
                                    key={key}
                                    name={key + ''}
                                    text={value}
                                    fontFamily={fontFamily}
                                    fontSize={fontSize}
                                    fill={color}
                                    onDragEnd={this.handleDragEnd}
                                    x={x}
                                    y={y}
                                    draggable
                                    onContextMenu={(e) => {
                                        this.handleContextMenu(e, key);
                                    }}
                                />
                            }
                        }[type]()
                    })}
                </Layer>
            </Stage>
        );
    }
}

DrawableArea = withEmit(DrawableArea);

const mapStateToProps = (state) => {
    return {
        latestCanvas: getLatestCanvasHistory(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateImage: (key, position, dimensions, saveInHistory) => dispatch(updateLogo(key, position, dimensions, saveInHistory)),
        cloneLatestHistoryState: () => dispatch(cloneLatestHistoryState()),
        showContextMenu: (position, options) => dispatch(showContextMenu(position, options)),
        deleteObject: (key) => dispatch(deleteObject(key))
    };
};

DrawableArea = connect(mapStateToProps, mapDispatchToProps)(DrawableArea);

export default DrawableArea;
