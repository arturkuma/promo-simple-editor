import React, {Component} from 'react';
import {Image} from 'react-konva';

type Props = {
	image: string
};

class CanvasImage extends Component<Props> {
	constructor(props) {
		super(props);

		this.state = {
			image: null
		};
	}

	componentDidMount() {
		this.updateImage();
	}

	componentDidUpdate({image}) {
		if (image !== this.props.image) {
			this.updateImage();
		}
	}

	updateImage = () => {
		const image = new window.Image();
		image.crossOrigin = 'Anonymous';
		image.src = this.props.image;

		image.onload = () => {
			this.setState({
				image: image
			});
		};
	};

	render() {
		return (
			<Image {...this.props} image={this.state.image}/>
		);
	}
}

export default CanvasImage;
