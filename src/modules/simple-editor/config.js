export const DEFAULT_BACKGROUND_URL = '/images/empty_background.bmp';

export const AVAILABLE_LOGOS = [
	'/images/logo/logo_one.png',
	'/images/logo/logo_two.png',
	'/images/logo/logo_three.png'
];

export const CANVAS_SIZE = {
	width: 400,
	height: 400
};
